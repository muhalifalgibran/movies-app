// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movies_mobx.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MoviesMobX on _MoviesMobXBase, Store {
  Computed<List<Movie>>? _$getListMovieComputed;

  @override
  List<Movie> get getListMovie => (_$getListMovieComputed ??=
          Computed<List<Movie>>(() => super.getListMovie,
              name: '_MoviesMobXBase.getListMovie'))
      .value;

  final _$moviesAtom = Atom(name: '_MoviesMobXBase.movies');

  @override
  ObservableList<Movie> get movies {
    _$moviesAtom.reportRead();
    return super.movies;
  }

  @override
  set movies(ObservableList<Movie> value) {
    _$moviesAtom.reportWrite(value, super.movies, () {
      super.movies = value;
    });
  }

  final _$selectedTagsAtom = Atom(name: '_MoviesMobXBase.selectedTags');

  @override
  ObservableList<String> get selectedTags {
    _$selectedTagsAtom.reportRead();
    return super.selectedTags;
  }

  @override
  set selectedTags(ObservableList<String> value) {
    _$selectedTagsAtom.reportWrite(value, super.selectedTags, () {
      super.selectedTags = value;
    });
  }

  final _$_MoviesMobXBaseActionController =
      ActionController(name: '_MoviesMobXBase');

  @override
  dynamic addMovies(Movie movie) {
    final _$actionInfo = _$_MoviesMobXBaseActionController.startAction(
        name: '_MoviesMobXBase.addMovies');
    try {
      return super.addMovies(movie);
    } finally {
      _$_MoviesMobXBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void deleteMovie(int movieId) {
    final _$actionInfo = _$_MoviesMobXBaseActionController.startAction(
        name: '_MoviesMobXBase.deleteMovie');
    try {
      return super.deleteMovie(movieId);
    } finally {
      _$_MoviesMobXBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic editMovie(Movie movie) {
    final _$actionInfo = _$_MoviesMobXBaseActionController.startAction(
        name: '_MoviesMobXBase.editMovie');
    try {
      return super.editMovie(movie);
    } finally {
      _$_MoviesMobXBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
movies: ${movies},
selectedTags: ${selectedTags},
getListMovie: ${getListMovie}
    ''';
  }
}
