import 'dart:ffi';

import 'package:mobx/mobx.dart';
import 'package:movies/model/movie.dart';

part 'movies_mobx.g.dart';

class MoviesMobX = _MoviesMobXBase with _$MoviesMobX;

abstract class _MoviesMobXBase with Store {
  @observable
  ObservableList<Movie> movies = ObservableList.of([
    Movie(
        id: 0,
        title: 'Spider-man: No Way Home',
        director: 'Jon Wats',
        summary: 'Spiderman from multiverse',
        tags: ['Sci-Fi']),
    Movie(
        id: 1,
        title: 'The 8th Night',
        director: 'Kim Tae-hyoung',
        summary:
            "With prayer beads in one hand and an axe in the other, a monk hunts down a millennia-old spirit that's possessing humans and unleashing hell on Earth.",
        tags: ['Horror']),
    Movie(
        id: 2,
        title: 'Red Notice',
        director: 'Rawson Marshall Thurber',
        summary:
            "In the world of international crime, an Interpol agent attempts to hunt down and capture the world's most wanted art thief.",
        tags: ['Action']),
    Movie(
        id: 3,
        title: 'Luca',
        director: 'Enrico Casarosal',
        summary:
            "Set in a beautiful seaside town on the Italian Riviera, the original animated feature is a coming-of-age story about one young boy experiencing an unforgettable summer filled with gelato, pasta and endless scooter rides",
        tags: ['Comedy']),
    Movie(
        id: 4,
        title: 'The Nutcracker and the Four Realms',
        director: 'Lasse Hallström, Joe Johnston',
        summary:
            "On Christmas Eve, Clara realises that before dying, her mother left her with an inheritance to a magical world of fairies and toy soldiers that are alive. She holds the key to the fate of this world.",
        tags: ['Comedy'])
  ]);

  @observable
  ObservableList<String> selectedTags = ObservableList.of([]);

  @action
  addMovies(Movie movie) {
    movies.add(movie);
  }

  @action
  void deleteMovie(int movieId) {
    movies.removeWhere((element) => element.id == movieId);
  }

  @action
  editMovie(Movie movie) {
    for (int x = 0; x < movies.length; x++) {
      if (movies[x].id == movie.id) {
        movies[x] = movie;
        break;
      }
    }
  }

  @computed
  List<Movie> get getListMovie => ObservableList.of(movies);
}
