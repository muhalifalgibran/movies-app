import 'package:flutter/material.dart';
import 'package:movies/presenters/list_movies.dart';
import 'package:movies/utils/router.dart';
import 'package:provider/provider.dart';
import 'package:movies/mobx/movies_mobx.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  final _appRouter = AppRouter();
  @override
  Widget build(BuildContext context) {
    return Provider<MoviesMobX>(
      create: (_) => MoviesMobX(),
      // dispose: (_, a) => a.dispose(),
      child: MaterialApp.router(
          debugShowCheckedModeBanner: false,
          routerDelegate: _appRouter.delegate(),
          title: 'Movie App',
          routeInformationParser: _appRouter.defaultRouteParser(),
          builder: (context, router) => router!),
    );
  }
}
