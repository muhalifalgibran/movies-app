import 'package:auto_route/src/router/auto_router_x.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';
import 'package:movies/model/movie.dart';
import 'package:movies/utils/router.dart';
import 'package:movies/utils/form_utils.dart';
import 'package:movies/mobx/movies_mobx.dart';
import 'package:provider/provider.dart';

class AddMovies extends StatefulWidget {
  const AddMovies({Key? key}) : super(key: key);

  @override
  State<AddMovies> createState() => _AddMoviesState();
}

class _AddMoviesState extends State<AddMovies> {
  String dropdownValue = 'Action';

  List<String> tags = [];

  final TextEditingController title = TextEditingController();

  final TextEditingController director = TextEditingController();

  final TextEditingController summary = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final moviesMobx = Provider.of<MoviesMobX>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        title: const Text('New Movies'),
        leading: GestureDetector(
          onTap: () {
            context.popRoute();
          },
          child: const Icon(Icons.arrow_back),
        ),
        backgroundColor: Colors.indigo,
        actions: [
          Row(
            children: [
              GestureDetector(
                  onTap: () {
                    final movie = Movie(
                      id: moviesMobx.movies.length + 1,
                      director: director.text,
                      summary: summary.text,
                      title: title.text,
                      tags: tags,
                    );

                    moviesMobx.addMovies(movie);
                    // moviesMobx.clearTags();

                    context.popRoute();
                  },
                  child: const Icon(Icons.save)),
            ],
          ),
          const SizedBox(
            width: 24,
          )
        ],
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 16),
        child: SingleChildScrollView(
          child: Column(
            children: [
              FormUtils.buildField(label: 'Title', controller: title),
              const SizedBox(
                height: 18,
              ),
              FormUtils.buildField(label: 'Director', controller: director),
              const SizedBox(
                height: 18,
              ),
              FormUtils.buildField(label: 'Summary', controller: summary),
              const SizedBox(
                height: 18,
              ),
              Observer(builder: (BuildContext context) {
                return SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: tags.length,
                      itemBuilder: (context, index) {
                        return Container(
                            margin: const EdgeInsets.all(3),
                            padding: const EdgeInsets.all(8),
                            decoration: BoxDecoration(
                                color: Colors.grey,
                                borderRadius: BorderRadius.circular(50)),
                            child: Row(
                              children: [
                                Text(
                                  tags[index],
                                  style: const TextStyle(color: Colors.white),
                                ),
                                GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        tags.removeWhere((element) =>
                                            element == tags[index]);
                                      });
                                    },
                                    child: const Icon(Icons.close,
                                        color: Colors.white)),
                              ],
                            ));
                      }),
                );
              }),
              const SizedBox(
                height: 18,
              ),
              SizedBox(
                width: double.infinity,
                child: DropdownButton<String>(
                  value: dropdownValue,
                  elevation: 16,
                  style: const TextStyle(color: Colors.deepPurple),
                  underline: Container(
                    height: 2,
                    color: Colors.deepPurpleAccent,
                  ),
                  onChanged: (String? newValue) {
                    setState(() {
                      dropdownValue = newValue!;
                      tags.add(newValue);
                    });
                  },
                  items: <String>[
                    'Action',
                    'Comedy',
                    'Fantasy',
                    'Horror',
                    'Sci-Fi',
                  ].map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
