import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:movies/mobx/movies_mobx.dart';
import 'package:movies/utils/router.dart';
import 'package:auto_route/auto_route.dart';
import 'package:provider/provider.dart';

class ListMovies extends StatefulWidget {
  const ListMovies({Key? key}) : super(key: key);

  @override
  State<ListMovies> createState() => _ListMoviesState();
}

class _ListMoviesState extends State<ListMovies> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final moviesMobx = Provider.of<MoviesMobX>(context, listen: false);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Movies'),
        backgroundColor: Colors.indigo,
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          context.pushRoute(const AddMoviesRoute());
        },
      ),
      body: Observer(
        builder: (BuildContext context) {
          return ListView.builder(
              itemCount: moviesMobx.getListMovie.length,
              itemBuilder: (context, index) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    () {
                      if (index == 0) {
                        return const SizedBox(
                          height: 16,
                        );
                      }
                      return Container();
                    }(),
                    GestureDetector(
                      onTap: () {
                        context.pushRoute(EditMoviesRoute(
                            movie: moviesMobx.getListMovie[index]));
                      },
                      child: Container(
                        margin: const EdgeInsets.symmetric(
                            vertical: 0, horizontal: 12),
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          elevation: 8,
                          child: Container(
                            width: double.infinity,
                            padding: const EdgeInsets.all(12),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  moviesMobx.getListMovie[index].title,
                                  style: const TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Text(moviesMobx.getListMovie[index].director),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                  ],
                );
              });
        },
      ),
    );
  }
}
