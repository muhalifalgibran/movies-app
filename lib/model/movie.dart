class Movie {
  final int id;
  final String title;
  final String director;
  final String summary;
  final List<String> tags;

  Movie({
    int? id,
    String? title,
    String? director,
    String? summary,
    List<String>? tags,
  })  : id = id ?? 0,
        title = title ?? '',
        director = director ?? '',
        summary = summary ?? '',
        tags = tags ?? [];
}
