// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

part of 'router.dart';

class _$AppRouter extends RootStackRouter {
  _$AppRouter([GlobalKey<NavigatorState>? navigatorKey]) : super(navigatorKey);

  @override
  final Map<String, PageFactory> pagesMap = {
    ListMoviesRoute.name: (routeData) {
      return MaterialPageX<dynamic>(
          routeData: routeData, child: const ListMovies());
    },
    AddMoviesRoute.name: (routeData) {
      return MaterialPageX<dynamic>(
          routeData: routeData, child: const AddMovies());
    },
    EditMoviesRoute.name: (routeData) {
      final args = routeData.argsAs<EditMoviesRouteArgs>();
      return MaterialPageX<dynamic>(
          routeData: routeData,
          child: EditMovies(key: args.key, movie: args.movie));
    }
  };

  @override
  List<RouteConfig> get routes => [
        RouteConfig(ListMoviesRoute.name, path: '/'),
        RouteConfig(AddMoviesRoute.name, path: '/add-movies'),
        RouteConfig(EditMoviesRoute.name, path: '/edit-movies')
      ];
}

/// generated route for
/// [ListMovies]
class ListMoviesRoute extends PageRouteInfo<void> {
  const ListMoviesRoute() : super(ListMoviesRoute.name, path: '/');

  static const String name = 'ListMoviesRoute';
}

/// generated route for
/// [AddMovies]
class AddMoviesRoute extends PageRouteInfo<void> {
  const AddMoviesRoute() : super(AddMoviesRoute.name, path: '/add-movies');

  static const String name = 'AddMoviesRoute';
}

/// generated route for
/// [EditMovies]
class EditMoviesRoute extends PageRouteInfo<EditMoviesRouteArgs> {
  EditMoviesRoute({Key? key, required Movie movie})
      : super(EditMoviesRoute.name,
            path: '/edit-movies',
            args: EditMoviesRouteArgs(key: key, movie: movie));

  static const String name = 'EditMoviesRoute';
}

class EditMoviesRouteArgs {
  const EditMoviesRouteArgs({this.key, required this.movie});

  final Key? key;

  final Movie movie;

  @override
  String toString() {
    return 'EditMoviesRouteArgs{key: $key, movie: $movie}';
  }
}
