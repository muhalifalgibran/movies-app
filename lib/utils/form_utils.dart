import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

typedef StringCallback = void Function(String data);

class NextForm {
  final FocusScopeNode scopeNode;
  final FocusNode nextNode;

  NextForm(this.scopeNode, this.nextNode);
}

class FormUtils {
  static TextFormField buildField({
    String? label,
    String? value,
    String? suffix,
    String? hint,
    bool? isError,
    bool isEnabled = true,
    bool obscureText = false,
    bool? filled,
    NextForm? nextForm,
    FocusNode? focusNode,
    StringCallback? onChanged,
    String? errorText,
    TextStyle? style,
    TextStyle? hintStyle,
    TextInputType? inputType,
    Icon? beforeIcon,
    Icon? prefixIcon,
    Widget? suffixIcon,
    double? borderRadius,
    Color? fillColor,
    EdgeInsetsGeometry? contentPadding,
    GestureTapCallback? onTap,
    FormFieldValidator<String>? validator,
    TextEditingController? controller,
    List<TextInputFormatter>? inputFormatters,
    double? cursorHeight,
    int? maxLength,
    bool? scaleLine = false,
  }) {
    final inputAction =
        nextForm == null ? TextInputAction.done : TextInputAction.next;
    return TextFormField(
      textInputAction: inputAction,
      enabled: isEnabled,
      initialValue: value,
      inputFormatters: inputFormatters,
      keyboardType: inputType,
      focusNode: focusNode,
      validator: validator,
      maxLength: maxLength,
      maxLines: () {
        if (scaleLine == true) return null;
        return 1;
      }(),
      // style: style ?? AppFont.lexendMedium16,
      controller: controller,
      onTap: onTap,
      // cursorColor: AppColor.primaryColorFont,
      onFieldSubmitted: (text) {
        if (nextForm != null) {
          nextForm.scopeNode.requestFocus(nextForm.nextNode);
        }
      },
      onChanged: onChanged,
      cursorHeight: cursorHeight ?? 19,
      obscureText: obscureText,
      decoration: InputDecoration(
        counterText: '',
        filled: filled,
        fillColor: fillColor,
        contentPadding: contentPadding ??
            const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
        icon: beforeIcon,
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(borderRadius ?? 10.0),
          borderSide: BorderSide(
            color: Colors.red,
          ),
        ),
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        errorText: errorText,
        hintText: hint,
        hintMaxLines: 1,
        labelText: label,
        alignLabelWithHint: false,
        isDense: true,
        suffixText: suffix,
        // hintStyle: hintStyle ?? AppFont.lexendRegular16Secondary,
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(borderRadius ?? 10.0),
          borderSide: const BorderSide(
            color: Colors.grey,
          ),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(borderRadius ?? 10.0),
          borderSide: const BorderSide(
            color: Colors.red,
          ),
        ),
        disabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(borderRadius ?? 10.0),
          borderSide: const BorderSide(
            color: Colors.grey,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(borderRadius ?? 10.0),
          borderSide: const BorderSide(
            color: Colors.grey,
          ),
        ),
      ),
    );
  }
}

extension on TextFormField {}
