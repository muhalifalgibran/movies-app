import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:movies/model/movie.dart';
import 'package:movies/presenters/add_movies.dart';
import 'package:movies/presenters/edit_movies.dart';
import 'package:movies/presenters/list_movies.dart';

part 'router.gr.dart';

// @CupertinoAutoRouter
// @AdaptiveAutoRouter
// @CustomAutoRouter
@MaterialAutoRouter(
  routes: <AutoRoute>[
    AutoRoute(page: ListMovies, initial: true),
    AutoRoute(page: AddMovies),
    AutoRoute(page: EditMovies),
  ],
)
class AppRouter extends _$AppRouter {}
